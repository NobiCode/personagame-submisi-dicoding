package com.example.myappsubmision

object GamesData {
    private val gameName = arrayOf(
        "Revelations: Persona",
        "Persona 2: Innocent Sin",
        "Persona 2: Eternal Punishment",
        "Persona 3 FES",
        "Persona 3: Dancing in Moonlight",
        "Persona 4 Golden",
        "Persona 4: Dancing All Night",
        "Persona 5 Royal",
        "Persona 5: Dancing in Starlight",
        "Persona 5 Scramble"
    )

    private val gameDetails = arrayOf(
        "Ceritanya berfokus pada sekelompok siswa sekolah menengah saat mereka dihadapkan pada serangkaian insiden supernatural. Setelah memainkan permainan meramal, masing-masing kelompok memperoleh kemampuan untuk memanggil Persona, banyak diri di dalamnya. Menggunakan kekuatan ini di bawah bimbingan Philemon, makhluk baik hati yang mewakili alam bawah sadar umat manusia, kelompok tersebut menghadapi berbagai kekuatan yang mengancam dunia. Gameplay berputar di sekitar karakter yang menavigasi lingkungan di sekitar kota mereka dan melawan musuh menggunakan Persona mereka. Selama permainan, pemain dapat membuat Persona baru untuk pertempuran menggunakan kartu mantra yang diperoleh dalam pertempuran atau dengan berbicara dengan musuh.",
        "Innocent Sin terjadi di kota fiksi Sumaru, berfokus pada sekelompok siswa SMA dari Seven Sisters High School. Protagonis, Tatsuya Suou, dan sekelompok teman harus menghadapi sosok jahat bernama Joker, yang menyebabkan penyebaran rumor yang membelokkan realitas ke seluruh kota. Dalam pencarian mereka, kelompok ini dibantu oleh Persona mereka, aspek yang dipersonifikasikan dari kepribadian mereka. Aspek gim ini menampilkan gameplay pertempuran berbasis giliran, di mana karakter menggunakan Persona mereka dalam pertempuran melawan iblis, dan sistem Rumor terpisah, di mana rumor yang tersebar di seluruh kota dapat memengaruhi peristiwa yang menguntungkan karakter.",
        "Eternal Punishment terjadi di kota fiksi Sumaru di Jepang, dan merupakan sekuel langsung dari Persona 2: Innocent Sin. Berlangsung beberapa bulan setelah Innocent Sin, game ini mengikuti reporter Maya Amano saat dia menyelidiki Kutukan Joker, sebuah fenomena jahat di mana keinginan dan rumor orang menjadi kenyataan dan menyebabkan kekacauan. Selama penyelidikannya, dia dan orang lain yang bergabung dengannya mendapatkan kemampuan untuk memanggil Persona, aspek yang dipersonifikasikan dari kepribadian mereka. Aspek gim ini menampilkan gameplay pertempuran berbasis giliran, di mana karakter menggunakan Persona mereka dalam pertempuran melawan iblis, dan sistem Rumor terpisah, di mana rumor yang tersebar di seluruh kota dapat memengaruhi peristiwa yang menguntungkan karakter.",
        "Di Persona 3, pemain mengambil peran sebagai siswa sekolah menengah laki-laki yang bergabung dengan Pasukan Eksekusi Ekstrakurikuler Khusus (SEES), sekelompok siswa yang menyelidiki Dark Hour, periode waktu antara satu hari dan hari berikutnya yang hanya sedikit orang sadari dari. Selama Dark Hour, pemain memasuki Tartarus, sebuah menara besar berisi Shadows, makhluk yang memakan pikiran manusia. Untuk melawan Bayangan, setiap anggota SEES dapat memanggil Persona, perwujudan dari batin seseorang. Fitur paling ikonik dari game ini adalah metode yang digunakan anggota SEES untuk melepaskan Persona mereka: dengan menembakkan benda mirip senjata yang disebut Evoker di kepala mereka. Selain elemen standar dari game role-playing, Persona 3 menyertakan elemen game simulasi, karena protagonis game tersebut berkembang dari hari ke hari melalui tahun ajaran, menjalin pertemanan, dan membentuk hubungan yang meningkatkan kekuatan Persona dalam pertempuran.",
        "Persona 3: Dancing in Moonlight [a] adalah permainan ritme yang dikembangkan dan diterbitkan oleh Atlus untuk PlayStation 4 dan PlayStation Vita. Membentuk bagian dari seri Persona — yang merupakan bagian dari franchise Megami Tensei yang lebih besar — game ini menampilkan pemeran utama dari video game role-playing tahun 2006 Persona 3. Gameplay berfokus pada karakter dari Persona 3 yang mengambil bagian dalam gameplay berbasis ritme yang diatur ke aslinya dan musik remix dari Persona 3. Dirilis di Jepang pada Mei 2018, dan di seluruh dunia pada Desember 2018.",
        "Plot Persona 4 terinspirasi oleh karya novelis misteri karena premis misteri pembunuhannya. Latar pedesaan didasarkan pada kota di pinggiran Gunung Fuji dan dimaksudkan sebagai \"tempat 'tidak ada tempat'\" dan merupakan tempat sentral untuk membuat pemain bersimpati dengan kehidupan sehari-hari para karakter. Pengembang menambahkan banyak acara dalam game untuk mencegah game menjadi basi. Selama pelokalan, banyak perubahan nama dan referensi budaya dilakukan untuk melestarikan efeknya melalui terjemahan, tetapi beberapa referensi budaya Jepang diubah atau dihapus. Perilisan game di Jepang disertai dengan merchandise seperti kostum karakter dan aksesoris. Paket permainan Amerika Utara dirilis dengan CD dengan musik pilihan dari permainan, dan, tidak seperti Persona 3, paket Eropa juga berisi CD soundtrack. Musik gim ini terutama ditulis oleh Shoji Meguro, dengan vokal dimainkan oleh Shihoko Hirata.",
        "Berlangsung kira-kira sebulan setelah epilog Persona 4 Golden, Rise Kujikawa, yang telah kembali ke industri idola, meminta Yu Narukami dan semua temannya yang lain untuk berdansa bersamanya di Love Meets Bonds Festival mendatang. Saat Yu, bersama dengan Naoto Shirogane, bergabung dengan Rise di studio tarinya dan diperkenalkan dengan sesama idola Kanami Mashita, mereka mengetahui bahwa grup idola Kanami, Kanamin Kitchen, secara misterius menghilang. Melihat rumor tentang video yang muncul di website LMB pada tengah malam yang diduga menampilkan idola yang sudah mati, Yu, Rise, dan Naoto melihat sendiri video tersebut dan tersedot ke dalam Midnight Stage, dunia yang terpisah dari dunia TV mereka. digunakannya.",
        "Persona 5 berlangsung di Tokyo modern dan mengikuti seorang siswa sekolah menengah yang dikenal dengan nama samaran Joker yang pindah ke sekolah baru setelah dituduh melakukan penyerangan dan menjalani masa percobaan. Selama satu tahun sekolah, dia dan siswa lainnya terbangun dengan kekuatan khusus, menjadi sekelompok penjaga rahasia yang dikenal sebagai Pencuri Hati Hantu. Mereka menjelajahi Metaverse, alam supernatural yang lahir dari keinginan bawah sadar manusia, untuk mencuri niat jahat dari hati orang dewasa. Seperti game sebelumnya dalam seri ini, party tersebut melawan musuh yang dikenal sebagai Shadows menggunakan manifestasi fisik dari jiwa mereka yang dikenal sebagai Persona mereka. Gim ini menggabungkan elemen role-playing dan dungeon crawl bersama skenario simulasi sosial.",
        "Persona 5: Dancing in Starlight [a] adalah permainan ritme yang dikembangkan dan diterbitkan oleh Atlus untuk PlayStation 4 dan PlayStation Vita. Membentuk bagian dari seri Persona — yang merupakan bagian dari franchise Megami Tensei yang lebih besar — game ini menampilkan pemeran utama dari video game role-playing tahun 2016 Persona 5. Gameplay berfokus pada karakter dari Persona 5 yang mengambil bagian dalam gameplay berbasis ritme yang disetel ke aslinya dan musik remix dari Persona 5. Dirilis di Jepang pada Mei 2018, dan di seluruh dunia pada Desember 2018.",
        "Permainan diatur enam bulan setelah peristiwa Persona 5, Protagonis dan Morgana kembali ke Tokyo untuk reuni dengan Pencuri Hati Phantom lainnya untuk menghabiskan liburan musim panas mereka bersama dalam perjalanan berkemah. Untuk menentukan persiapan camping, mereka menggunakan aplikasi populer bernama EMMA. Saat pergi ke Shibuya untuk membeli peralatan yang diperlukan, mereka melewati idola yang sedang naik daun, Alice Hiiragi, yang memberi protagonis sebuah kartu yang meminta mereka untuk memasukkan \"Wonderland\" ke dalam aplikasi EMMA untuk acara istimewanya. Setelah memasukkan kata kunci, bagaimanapun, dia, Morgana dan Ryuji dipindahkan ke versi alternatif misterius Tokyo yang disebut Penjara, di mana mereka bertemu dengan penguasa bernama Raja yang merupakan diri Bayangan Alice, dan A.I. bernama Sophia yang mampu melawan Shadows yang bergabung dengan party mereka. Mereka mengetahui bahwa EMMA, mirip dengan Metaverse Navigator, memungkinkan mereka untuk masuk ke Penjara. Mendengar rumor bahwa orang-orang telah diserang oleh Shadows in Jails yang membuat mereka bertingkah aneh di dunia nyata, Joker dan kawan-kawan sekali lagi mendirikan Phantom Thieves of Hearts."
    )

    private val gameWriter = arrayOf(
        "Tadashi Satomi",
        "Tadashi Satomi, Kazuyuki Yamai (PSP)",
        "Tadashi Satomi",
        "Yuichiro Tanaka",
        "Yuichiro Tanaka, Nobuyoshi Miwa",
        "Shigenori Soejima, Susumu Nishizawa",
        "Teppei Kobayashi",
        "Shinji Yamamoto, Yuichiro Tanaka, Katsura Hashino",
        "Shinji Yamamoto, Yuichiro Tanaka, Katsura Hashino",
        "Takaaki Ogata, Toru Yorogi, Yusuke Nitta"
    )

    private val gameRelease = arrayOf(
        "September 20, 1996",
        "June 24, 1999",
        "June 29, 2000",
        "July 13, 2006",
        "May 24, 2018",
        "July 10, 2008",
        "June 25, 2015",
        "September 15, 2016",
        "May 24, 2018",
        "February 20, 2020"
    )

    private val gamePlatform = arrayOf(
        "PlayStation, Microsoft Windows, PlayStation Portable",
        "PlayStation, PlayStation Portable",
        "PlayStation, PlayStation Portable",
        "PlayStation 2, PlayStation Portable",
        "PlayStation 4, PlayStation Vita",
        "PlayStation 2, PlayStation Vita, Microsoft Windows",
        "PlayStation Vita, PlayStation 4",
        "PlayStation 3, PlayStation 4",
        "PlayStation 4, PlayStation Vita",
        "Nintendo Switch, PlayStation 4"

    )

    private val gamesImages = intArrayOf(
        R.drawable.smt_rp,
        R.drawable.p2is,
        R.drawable.p2ep,
        R.drawable.p3fes,
        R.drawable.p3dim,
        R.drawable.p4g,
        R.drawable.p4dan,
        R.drawable.p5r,
        R.drawable.p5dis,
        R.drawable.p5s
    )

    val listData: ArrayList<Game> get() {
        val  list = arrayListOf<Game>()
        for (position in gameName.indices){
            val game = Game()
            game.name = gameName[position]
            game.detail = gameDetails[position]
            game.photo = gamesImages[position]
            game.writer = gameWriter[position]
            game.release = gameRelease[position]
            game.platform = gamePlatform[position]

            list.add(game)
        }
        return list
    }
}