package com.example.myappsubmision

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class ProfilActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil)

        val backButton = supportActionBar

        backButton!!.setDisplayHomeAsUpEnabled(true)
        backButton.setDisplayShowHomeEnabled(true)
        backButton.title = "About Me"
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}