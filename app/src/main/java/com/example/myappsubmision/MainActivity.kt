package com.example.myappsubmision

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var rvGames: RecyclerView
    private var list: ArrayList<Game> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = "Mini Wiki Persona"

        rvGames = findViewById(R.id.rv_games)
        rvGames.setHasFixedSize(true)

        list.addAll(GamesData.listData)
        showRecyclerList()
    }


    private fun showRecyclerList() {
        rvGames.layoutManager = LinearLayoutManager(this)
        val listGameAdapter = ListGameAdapter(list)
        rvGames.adapter = listGameAdapter

        listGameAdapter.setOnItemClick(object : ListGameAdapter.OnItemClick{
            override fun onItemClicked(data: Game) {
                showSelectedGame(data)
            }
        })
    }

    private fun showRecycleGrid(){
        rvGames.layoutManager = GridLayoutManager(this,2)
        val gridGameAdapter = GridGameAdapter(list)
        rvGames.adapter = gridGameAdapter

        gridGameAdapter.setOnItemClick(object : GridGameAdapter.OnItemClick{
            override fun onItemClicked(data: Game) {
                showSelectedGame(data)
            }
        })
    }

    private fun showSelectedGame(game: Game){
        Toast.makeText(this, "kamu memilih "+ game.name, Toast.LENGTH_SHORT).show()

        var title: String = game.name
        var desc: String = game.detail
        var image: Int = game.photo
        var writer: String = game.writer
        var release: String = game.release
        var platform: String = game.platform

        val moveDetail = Intent(this@MainActivity, DetailActivity::class.java)
        moveDetail.putExtra(DetailActivity.EXTRA_TITLE, title)
        moveDetail.putExtra(DetailActivity.EXTRA_DESC, desc)
        moveDetail.putExtra(DetailActivity.EXTRA_IMG, image)
        moveDetail.putExtra(DetailActivity.EXTRA_WRITE, writer)
        moveDetail.putExtra(DetailActivity.EXTRA_YEAR, release)
        moveDetail.putExtra(DetailActivity.EXTRA_PLAT, platform)

        startActivity(moveDetail)
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setMode(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun setMode(selectMode: Int) {
        when (selectMode){
            R.id.btn_profile -> {
                val moveProfil = Intent(this@MainActivity, ProfilActivity::class.java)
                startActivity(moveProfil)
            }
            R.id.btn_grid -> {
                showRecycleGrid()
            }
            R.id.btn_list -> {
                showRecyclerList()
            }
        }
    }

}