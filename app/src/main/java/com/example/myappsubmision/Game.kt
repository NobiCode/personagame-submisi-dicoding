package com.example.myappsubmision

data class Game(
    var name: String = "",
    var detail: String = "",
    var photo: Int = 0,
    var writer: String = "",
    var release: String = "",
    var platform: String = ""
)