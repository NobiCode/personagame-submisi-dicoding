package com.example.myappsubmision

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView

class DetailActivity : AppCompatActivity() {
    companion object{
        const val EXTRA_TITLE = "extra_title"
        const val EXTRA_DESC = "extra_desc"
        const val EXTRA_IMG = "extra_img"
        const val EXTRA_WRITE = "extra_write"
        const val EXTRA_YEAR = "extra_year"
        const val EXTRA_PLAT = "extra_plat"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val backButton = supportActionBar

        backButton!!.setDisplayHomeAsUpEnabled(true)
        backButton.setDisplayShowHomeEnabled(true)
        backButton.title = "Detail Series"

        val tvDataRecivedTitle: TextView = findViewById(R.id.tv_data_recived_title)
        val tvDataRecivedDesc: TextView = findViewById(R.id.tv_data_recived_desc)
        val tvDataRecivedImage: ImageView = findViewById(R.id.tv_data_recived_image)
        val tvDataRecivedWriter: TextView = findViewById(R.id.set_penulis)
        val tvDataRecivedRelease: TextView = findViewById(R.id.set_rilis)
        val tvDataRecivedPlatform: TextView = findViewById(R.id.set_platform)

        val title = intent.getStringExtra(EXTRA_TITLE)
        val desc = intent.getStringExtra(EXTRA_DESC)
        val image = intent.getIntExtra(EXTRA_IMG, 0)
        val write = intent.getStringExtra(EXTRA_WRITE)
        val release = intent.getStringExtra(EXTRA_YEAR)
        val platform = intent.getStringExtra(EXTRA_PLAT)

        tvDataRecivedTitle.text = title
        tvDataRecivedDesc.text = desc
        tvDataRecivedImage.setImageResource(image)
        tvDataRecivedWriter.text = write
        tvDataRecivedRelease.text = release
        tvDataRecivedPlatform.text = platform
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}